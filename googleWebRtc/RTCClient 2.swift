//
//  RTCClient.swift
//  WebRTCClient
//
//  Created by Евгений on 09/01/2019.
//  Copyright © 2019 Momotov. All rights reserved.
//

import Foundation
import WebRTC

public enum RTCClientState {
    case disconnected
    case connecting
    case connected
}

protocol RTCClientDelegate: class {
    func rtcClient(client : RTCClient, startCallWithSdp sdp: String)
    func rtcClient(client : RTCClient, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack)
    func rtcClient(client : RTCClient, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack)
    func rtcClient(client : RTCClient, didReceiveError error: Error)
    func rtcClient(client : RTCClient, didChangeConnectionState connectionState: RTCIceConnectionState)
    func rtcClient(client : RTCClient, didChangeState state: RTCClientState)
    func rtcClient(client : RTCClient, didGenerateIceCandidate iceCandidate: RTCIceCandidate)
    func rtcClient(client : RTCClient, didCreateLocalCapturer capturer: RTCCameraVideoCapturer)
}

class RTCClient: NSObject {
    
    weak var delegate: RTCClientDelegate?
    
    var connectionFactory: RTCPeerConnectionFactory!
    var iceServers: [RTCIceServer]! // Список STUN и TURN серверов
    var peerConnection: RTCPeerConnection!
    
    var defaultOfferConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio": "true"], optionalConstraints: nil)
    var defaultConnectionConstraint = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: ["DtlsSrtpKeyAgreement": "true"])
    
    var mediaConstraint: RTCMediaConstraints {
        let constraints = ["minWidth": "0", "minHeight": "0", "maxWidth" : "480", "maxHeight": "640"]
        return RTCMediaConstraints(mandatoryConstraints: constraints, optionalConstraints: nil)
    }
    
    var state: RTCClientState = .connecting {
        didSet {
            self.delegate?.rtcClient(client: self, didChangeState: state)
        }
    }
    
    override init() {
        super.init()
    }
    
    convenience init(iceServers: [RTCIceServer]) {
        self.init()
        self.iceServers = iceServers
        self.configure()
    }
    
    func configure() {
        initializePeerConnectionFactory()
        initializePeerConnection()
    }
    
    func initializePeerConnectionFactory() {
        RTCPeerConnectionFactory.initialize()
        connectionFactory = RTCPeerConnectionFactory()
    }
    
    func initializePeerConnection() {
        let configuration = RTCConfiguration()
        configuration.iceServers = iceServers
        peerConnection = connectionFactory.peerConnection(with: configuration, constraints: defaultConnectionConstraint, delegate: self)
    }
    
    func startConnection() {
        guard let peerConnection = peerConnection else {
            return
        }
        state = .connecting
        let localStream = self.localStream()
        peerConnection.add(localStream)
    }
    
    func localStream() -> RTCMediaStream {
        let factory = connectionFactory!
        let localStream = factory.mediaStream(withStreamId: "RTCmS")
        let audioTrack = factory.audioTrack(withTrackId: "RTCaS0")
        localStream.addAudioTrack(audioTrack)
        
        return localStream
    }
    
    func makeOffer() {
        guard let peerConnection = peerConnection else {
            return
        }
        peerConnection.offer(for: defaultConnectionConstraint) { (sdp, error) in
            if let error = error {
                self.delegate?.rtcClient(client: self, didReceiveError: error as NSError)
            } else {
                self.handleSdpGenerated(sdpDescription: sdp)
            }
        }
    }
    
    func handleSdpGenerated(sdpDescription: RTCSessionDescription?) {
        guard let sdpDescription = sdpDescription else {
            return
        }
        peerConnection.setLocalDescription(sdpDescription) { [weak self] (error) in
            guard let this= self, let error = error else {
                
            }
        }
    }
    
}

extension RTCClient: RTCPeerConnectionDelegate {
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        
    }
    
}
