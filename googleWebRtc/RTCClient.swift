//
//  RTCClient.swift
//  WebRTCClient
//
//  Created by Евгений on 09/01/2019.
//  Copyright © 2019 Momotov. All rights reserved.
//

import Foundation
import WebRTC

public enum RTCClientState {
    case disconnected
    case connecting
    case connected
}

protocol RTCClientDelegate: class {
    func rtcClient(client : RTCClient, startCallWithSdp sdp: String)
    func rtcClient(client : RTCClient, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack)
    func rtcClient(client : RTCClient, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack)
    func rtcClient(client : RTCClient, didReceiveError error: Error)
    func rtcClient(client : RTCClient, didChangeConnectionState connectionState: RTCIceConnectionState)
    func rtcClient(client : RTCClient, didChangeState state: RTCClientState)
    func rtcClient(client : RTCClient, didGenerateIceCandidate iceCandidate: RTCIceCandidate)
    func rtcClient(client : RTCClient, didCreateLocalCapturer capturer: RTCCameraVideoCapturer)
}

extension RTCClientDelegate {
    
    func rtcClient(client : RTCClient, didReceiveError error: Error) {
        
    }
    
    func rtcClient(client : RTCClient, didChangeConnectionState connectionState: RTCIceConnectionState) {
        
    }
    
    func rtcClient(client : RTCClient, didChangeState state: RTCClientState) {
        
    }
}

class RTCClient: NSObject {
    
    weak var delegate: RTCClientDelegate?
    
    var connectionFactory: RTCPeerConnectionFactory = RTCPeerConnectionFactory()
    var iceServers: [RTCIceServer] = [] // Список STUN и TURN серверов
    var peerConnection: RTCPeerConnection?
    var remoteIceCandidates: [RTCIceCandidate] = []
    
    let audioCallConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio" : "true"],
                                                  optionalConstraints: nil)
    var defaultOfferConstraint = RTCMediaConstraints(mandatoryConstraints: ["OfferToReceiveAudio": "true"], optionalConstraints: nil)
    var defaultConnectionConstraint = RTCMediaConstraints(mandatoryConstraints: nil, optionalConstraints: ["DtlsSrtpKeyAgreement": "true"])
    
    var mediaConstraint: RTCMediaConstraints {
        let constraints = ["minWidth": "0", "minHeight": "0", "maxWidth" : "480", "maxHeight": "640"]
        return RTCMediaConstraints(mandatoryConstraints: constraints, optionalConstraints: nil)
    }
    
    var state: RTCClientState = .connecting {
        didSet {
            self.delegate?.rtcClient(client: self, didChangeState: state)
        }
    }
    
    override init() {
        super.init()
    }
    
    convenience init(iceServers: [RTCIceServer]) {
        self.init()
        self.iceServers = iceServers
        self.configure()
    }
    
    func configure() {
        initializePeerConnectionFactory()
        initializePeerConnection()
    }
    
    func initializePeerConnectionFactory() {
        print("Initialize Peer Connection Factory")
        RTCPeerConnectionFactory.initialize()
        connectionFactory = RTCPeerConnectionFactory()
    }
    
    func initializePeerConnection() {
        print("Initialize Peer Connection")
        let configuration = RTCConfiguration()
        configuration.iceServers = iceServers
        peerConnection = connectionFactory.peerConnection(with: configuration, constraints: defaultConnectionConstraint, delegate: self)
    }
    
    func startConnection() {
        print("Start connection")
        guard let peerConnection = peerConnection else {
            print("Not peer connection")
            return
        }
        state = .connecting
        let localStream = self.localStream()
        peerConnection.add(localStream)
        if let localVideoTrack = localStream.videoTracks.first {
            print("Local Video Track")
            delegate?.rtcClient(client: self, didReceiveLocalVideoTrack: localVideoTrack)
        }
    }
    
    func localStream() -> RTCMediaStream {
        let factory = connectionFactory
        let localStream = factory.mediaStream(withStreamId: "RTCmS")
        
        if !AVCaptureState.isVideoDisabled {
            let videoSource = factory.videoSource()
            let capturer = RTCCameraVideoCapturer(delegate: videoSource)
            delegate?.rtcClient(client: self, didCreateLocalCapturer: capturer)
            let videoTrack = factory.videoTrack(with: videoSource, trackId: "RTCvS0")
            videoTrack.isEnabled = true
            localStream.addVideoTrack(videoTrack)
        } else {
            let error = NSError.init(domain: ErrorDomain.videoPermissionDenied, code: 0, userInfo: nil)
            self.delegate?.rtcClient(client: self, didReceiveError: error)
        }
        
        if !AVCaptureState.isAudioDisabled {
            let audioTrack = factory.audioTrack(withTrackId: "RTCaS0")
            localStream.addAudioTrack(audioTrack)
        } else {
            let error = NSError.init(domain: ErrorDomain.audioPermissionDenied, code: 0, userInfo: nil)
            self.delegate?.rtcClient(client: self, didReceiveError: error)
        }
        
        return localStream
    }
    
    func makeOffer() {
        print("Make offer")
        guard let peerConnection = peerConnection else {
            print("Not peer connection")
            return
        }
        peerConnection.offer(for: defaultConnectionConstraint) { (sdp, error) in
            if let error = error {
                self.delegate?.rtcClient(client: self, didReceiveError: error as NSError)
                print("Error: \(error)")
            } else {
                self.handleSdpGenerated(sdpDescription: sdp)
            }
        }
    }
    
    func handleSdpGenerated(sdpDescription: RTCSessionDescription?) {
        print("Handle SDP Generated")
        guard let sdpDescription = sdpDescription else {
            return
        }
        peerConnection?.setLocalDescription(sdpDescription) { [weak self] (error) in
            guard let this = self, let error = error else { return }
            this.delegate?.rtcClient(client: this, didReceiveError: error)
        }
        print("Start call with SDP")
        delegate?.rtcClient(client: self, startCallWithSdp: sdpDescription.sdp)
    }
    
    func createAnswerForOfferReceived(withRemoteSdp remoteSdp: String?) {
        guard let remoteSdp = remoteSdp, let peerConnection = peerConnection else {
            return
        }
        let sessionDescription = RTCSessionDescription(type: .offer, sdp: remoteSdp)
        peerConnection.setRemoteDescription(sessionDescription) { [weak self] (error) in
            guard let this = self else { return }
            if let error = error {
                this.delegate?.rtcClient(client: this, didReceiveError: error)
            } else {
                this.handleRemoteDescriptionSet()
                peerConnection.answer(for: this.audioCallConstraint
                    , completionHandler: { (sdp, error) in
                        if let error = error {
                            this.delegate?.rtcClient(client: this, didReceiveError: error)
                        } else {
                            this.handleSdpGenerated(sdpDescription: sdp)
                            this.state = .connected
                        }
                })
            }
        }
    }
    
    public func handleAnswerReceived(withRemoteSDP remoteSdp: String?) {
        guard let remoteSdp = remoteSdp else {
            return
        }
        let sessionDescription = RTCSessionDescription.init(type: .answer, sdp: remoteSdp)
        self.peerConnection?.setRemoteDescription(sessionDescription, completionHandler: { [weak self] (error) in
            guard let this = self else { return }
            if let error = error {
                this.delegate?.rtcClient(client: this, didReceiveError: error)
            } else {
                this.handleRemoteDescriptionSet()
                this.state = .connected
            }
        })
    }
    
    func handleRemoteDescriptionSet() {
        for iceCandidate in remoteIceCandidates {
            peerConnection?.add(iceCandidate)
        }
        remoteIceCandidates = []
    }
    
}

extension RTCClient: RTCPeerConnectionDelegate {
    
    func peerConnectionShouldNegotiate(_ peerConnection: RTCPeerConnection) {
        
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange stateChanged: RTCSignalingState) {
        print("Did change")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didAdd stream: RTCMediaStream) {
        print("Did Add")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove stream: RTCMediaStream) {
        print("Did remove")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceConnectionState) {
        print("Did change")
        self.delegate?.rtcClient(client: self, didChangeConnectionState: newState)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didChange newState: RTCIceGatheringState) {
        print("Did change 2")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didGenerate candidate: RTCIceCandidate) {
        print("Did generate")
        self.delegate?.rtcClient(client: self, didGenerateIceCandidate: candidate)
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didRemove candidates: [RTCIceCandidate]) {
        print("Did remove")
    }
    
    func peerConnection(_ peerConnection: RTCPeerConnection, didOpen dataChannel: RTCDataChannel) {
        print("Did OPen")
    }
    
}

public struct ErrorDomain {
    public static let videoPermissionDenied = "Video permission denied"
    public static let audioPermissionDenied = "Audio permission denied"
}
