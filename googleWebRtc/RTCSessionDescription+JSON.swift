//
//  RTCSessionDescription+JSON.swift
//  googleWebRtc
//
//  Created by Евгений on 09/01/2019.
//  Copyright © 2019 Momotov. All rights reserved.
//

import Foundation
import WebRTC

extension RTCSessionDescription {
    
    func jsonString() -> String? {
        let dict = [
            CodingKeys.sdp.rawValue: self.sdp,
            CodingKeys.type.rawValue: self.type.rawValue,
            ] as [String : Any?]
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted),
            let jsonString = String(data: jsonData, encoding: .utf8)  {
            return jsonString
        }
        return nil
    }
    
    class func fromJsonString(_ string: String) -> RTCSessionDescription? {
        if let data = string.data(using: .utf8),
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
            let jsonDictionary = jsonObject  as? [String: Any?],
            let sdp = jsonDictionary[CodingKeys.sdp.rawValue] as? String ,
            let typeNumber = jsonDictionary[CodingKeys.type.rawValue] as? Int,
            let type = RTCSdpType(rawValue: typeNumber) {
            return RTCSessionDescription(type: type, sdp: sdp)
        }
        
        return nil
    }
    
    enum CodingKeys: String, CodingKey {
        case sdp
        case type
    }
}

extension RTCIceCandidate {
    
    func jsonString() -> String? {
        let dict = [
            CodingKeys.sdp.rawValue: self.sdp,
            CodingKeys.sdpMid.rawValue: self.sdpMid,
            CodingKeys.sdpMLineIndex.rawValue: self.sdpMLineIndex
            ] as [String : Any?]
        
        if let jsonData = try? JSONSerialization.data(withJSONObject: dict, options: .prettyPrinted),
            let jsonString = String(data: jsonData, encoding: .utf8)  {
            return jsonString
        }
        return nil
    }
    
    class func fromJsonString(_ string: String) -> RTCIceCandidate? {
        if let data = string.data(using: .utf8),
            let jsonObject = try? JSONSerialization.jsonObject(with: data, options: []),
            let jsonDictionary = jsonObject  as? [String: Any?],
            let sdp = jsonDictionary[CodingKeys.sdp.rawValue] as? String ,
            let sdpMid = jsonDictionary[CodingKeys.sdpMid.rawValue] as? String?,
            let sdpMLineIndex = jsonDictionary[CodingKeys.sdpMLineIndex.rawValue] as? Int32{
            return RTCIceCandidate(sdp: sdp, sdpMLineIndex: sdpMLineIndex, sdpMid: sdpMid)
        }
        
        return nil
    }
    
    enum CodingKeys: String, CodingKey {
        case sdp
        case sdpMLineIndex
        case sdpMid
    }
}

extension RTCIceConnectionState: CustomStringConvertible {
    public var description: String {
        switch self {
        case .new:          return "new"
        case .checking:     return "checking"
        case .connected:    return "connected"
        case .completed:    return "completed"
        case .failed:       return "failed"
        case .disconnected: return "disconnected"
        case .closed:       return "closed"
        case .count:        return "count"
        }
    }
}

extension RTCSignalingState: CustomStringConvertible {
    public var description: String {
        switch self {
        case .stable:               return "stable"
        case .haveLocalOffer:       return "haveLocalOffer"
        case .haveLocalPrAnswer:    return "haveLocalPrAnswer"
        case .haveRemoteOffer:      return "haveRemoteOffer"
        case .haveRemotePrAnswer:   return "haveRemotePrAnswer"
        case .closed:               return "closed"
        }
    }
}

extension RTCIceGatheringState: CustomStringConvertible {
    public var description: String {
        switch self {
        case .new:          return "new"
        case .gathering:    return "gathering"
        case .complete:     return "complete"
        }
    }
}
