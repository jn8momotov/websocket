//
//  SignalClient.swift
//  googleWebRtc
//
//  Created by Евгений on 09/01/2019.
//  Copyright © 2019 Momotov. All rights reserved.
//

import UIKit
import WebRTC
import Starscream

protocol SignalClientDelegate: class {
    func signalClientDidConnect(_ signalClient: SignalClient)
    func signalClientDidDisconnect(_ signalClient: SignalClient, error: Error?)
    func signalClient(_ signalClient: SignalClient, didReceiveRemoteSdp sdp: RTCSessionDescription)
    func signalClient(_ signalClient: SignalClient, didReceiveCandidate candidate: RTCIceCandidate)
}

struct Message: Codable {
    enum PayloadType: String, Codable {
        case sdp, candidate
    }
    let type: PayloadType
    let payload: String
}

class SignalClient {
    
    private let serverUrl = "ws://94.250.250.224:8088"
    private let socket: WebSocket
    weak var delegate: SignalClientDelegate?
    
    init() {
        self.socket = WebSocket(url: URL(string: self.serverUrl)!)
        socket.disableSSLCertValidation = true
    }
    func connect() {
        self.socket.delegate = self
        self.socket.connect()
    }
    
    func send(sdp: RTCSessionDescription) {
        let message = Message(type: .sdp, payload: sdp.jsonString() ?? "")
        if let dataMessage = try? JSONEncoder().encode(message),
            let stringMessage = String(data: dataMessage, encoding: .utf8) {
            self.socket.write(string: stringMessage)
        }
    }
    
    func send(candidate: RTCIceCandidate) {
        let message = Message(type: .candidate,
                              payload: candidate.jsonString() ?? "")
        if let dataMessage = try? JSONEncoder().encode(message),
            let stringMessage = String(data: dataMessage, encoding: .utf8){
            self.socket.write(string: stringMessage)
        }
    }
}


extension SignalClient: WebSocketDelegate {
    func websocketDidConnect(socket: WebSocketClient) {
        self.delegate?.signalClientDidConnect(self)
    }
    
    func websocketDidDisconnect(socket: WebSocketClient, error: Error?) {
        self.delegate?.signalClientDidDisconnect(self, error: error)
        
        // try to reconnect every two seconds
        DispatchQueue.global().asyncAfter(deadline: .now() + 2) {
            print("Trying to reconnect to signaling server...")
            self.socket.connect()
        }
    }
    
    func websocketDidReceiveMessage(socket: WebSocketClient, text: String) {
        guard let data = text.data(using: .utf8),
            let message = try? JSONDecoder().decode(Message.self, from: data) else {
                return
        }
        
        switch message.type {
        case .candidate:
            if let candidate = RTCIceCandidate.fromJsonString(message.payload) {
                self.delegate?.signalClient(self, didReceiveCandidate: candidate)
            }
        case .sdp:
            if let sdp = RTCSessionDescription.fromJsonString(message.payload) {
                self.delegate?.signalClient(self, didReceiveRemoteSdp: sdp)
            }
        }
    }
    
    func websocketDidReceiveData(socket: WebSocketClient, data: Data) {
        
    }
}
