//
//  ViewController.swift
//  WebRTCClient
//
//  Created by Евгений on 09/01/2019.
//  Copyright © 2019 Momotov. All rights reserved.
//

import UIKit
import WebRTC

class ViewController: UIViewController {
    
    var signalClient = SignalClient()
    var videoClient: RTCClient?
    var captureController: RTCCapturer!
    
    var localVideoTrack: RTCVideoTrack?
    var remoteVideoTrack: RTCVideoTrack?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        signalClient.connect()
        signalClient.delegate = self
        
        let iceServer = RTCIceServer(urlStrings: ["stun:stun.l.google.com:19302"])
        let client = RTCClient(iceServers: [iceServer])
        client.delegate = self
        videoClient = client
        client.startConnection()
        
        
        
    }

}

extension ViewController: RTCClientDelegate {
    
    func rtcClient(client: RTCClient, didCreateLocalCapturer capturer: RTCCameraVideoCapturer) {
        print("Did create local capturer")
        if UIDevice.current.model != "Simulator" {
            print("Its not simulator")
            let settingsModel = RTCCapturerSettingsModel()
            self.captureController = RTCCapturer.init(withCapturer: capturer, settingsModel: settingsModel)
            captureController.startCapture()
        } else {
            print("Its simulator!")
        }
    }
    
    func rtcClient(client : RTCClient, didReceiveError error: Error) {
        // Error Received
        print("Error: \(error.localizedDescription)")
    }
    
    func rtcClient(client : RTCClient, didGenerateIceCandidate iceCandidate: RTCIceCandidate) {
        // iceCandidate generated, pass this to other user using any signal method your app uses
        print("Did generate ice candidates")
    }
    
    func rtcClient(client : RTCClient, startCallWithSdp sdp: String) {
        // SDP generated, pass this to other user using any signal method your app uses
        print("Start call!!!")
    }
    
    func rtcClient(client : RTCClient, didReceiveLocalVideoTrack localVideoTrack: RTCVideoTrack) {
        // Use localVideoTrack generated for rendering stream to remoteVideoView
        print("did Recieve local video track")
    }
    func rtcClient(client : RTCClient, didReceiveRemoteVideoTrack remoteVideoTrack: RTCVideoTrack) {
        // Use remoteVideoTrack generated for rendering stream to remoteVideoView
        print("Did remote video track")
    }
    
}

extension ViewController: SignalClientDelegate {
    
    func signalClientDidConnect(_ signalClient: SignalClient) {
        print("Did connect")
    }
    
    func signalClientDidDisconnect(_ signalClient: SignalClient, error: Error?) {
        print("Did disconnect")
        print("Error: \(error?.localizedDescription)")
    }
    
    func signalClient(_ signalClient: SignalClient, didReceiveRemoteSdp sdp: RTCSessionDescription) {
        print("Did receive remote sdp")
    }
    
    func signalClient(_ signalClient: SignalClient, didReceiveCandidate candidate: RTCIceCandidate) {
        print("Did receive candidate")
    }
    
}

